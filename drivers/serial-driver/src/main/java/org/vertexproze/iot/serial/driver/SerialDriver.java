/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package org.vertexproze.iot.serial.driver;

import com.fazecast.jSerialComm.SerialPort;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.vertexprize.iot.container.ObjectContainer;
import org.vertexprize.iot.driver.api.DriverAPI;

/**
 * Driver for serial driver (USB, RS232C...)
 *
 * @author vaganovdv
 */
public class SerialDriver implements DriverAPI {

    private  List<SerialPort> comportList = new ArrayList<>();
    private int index;
    
    @Override
    public ObjectContainer init() {
     
        ObjectContainer container = new ObjectContainer();        
        SerialPort[] commPorts = SerialPort.getCommPorts();
        comportList = Arrays.asList(commPorts);

        
        System.out.println("Список портов ["+comportList.size()+"] записей");
        StringBuilder sb = new StringBuilder();
        
        index = 0;
        comportList.stream().forEach( port -> {
            index++;
            sb.append(String.format("%8s", "["+index+"]"));
            sb.append(String.format("%30s", port.getPortDescription()));
            sb.append(String.format("%30s", port.getSystemPortName()));
            sb.append(String.format("%30s", port.getSystemPortPath()));
            sb.append(String.format("%30s", port.getVendorID()));
            sb.append("\n");   
            
        });
        
        System.out.println(sb.toString());
        
        
        return container;
        
    }

    @Override
    public ObjectContainer open() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ObjectContainer close() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ObjectContainer write(Object device, Object protocolMessage) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ObjectContainer read(Object device) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String getUuid() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void setUuid(String uuid) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void setName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
   
   
}
